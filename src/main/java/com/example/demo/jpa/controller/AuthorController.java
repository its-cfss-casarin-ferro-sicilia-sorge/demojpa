package com.example.demo.jpa.controller;

import com.example.demo.jpa.model.Author;
import com.example.demo.jpa.repository.AuthorRepository;
import com.turkraft.springfilter.boot.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authors")
class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping
    public ResponseEntity<Page<Author>> getAuthors(
        @Filter Specification<Author> specification,
        Pageable pageable
    ) {
        return ResponseEntity.ok(authorRepository.findAll(specification, pageable));
    }

    @GetMapping("author")
    public ResponseEntity<Author> getAuthor(@Filter Specification<Author> spec) {
        return ResponseEntity.of(authorRepository.findOne(spec));
    }

}
