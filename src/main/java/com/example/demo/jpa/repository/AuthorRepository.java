package com.example.demo.jpa.repository;

import com.example.demo.jpa.model.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

import static com.example.demo.jpa.repository.GraphName.AUTHOR_DETAIL;
import static com.example.demo.jpa.repository.GraphName.AUTHOR_LIST;

public interface AuthorRepository extends JpaRepository<Author, Long>,
    JpaSpecificationExecutor<Author> {

    @Override
    @EntityGraph(value = AUTHOR_LIST, type = EntityGraph.EntityGraphType.LOAD)
    Page<Author> findAll(Pageable pageable);

    @Override
    @EntityGraph(value = AUTHOR_LIST, type = EntityGraph.EntityGraphType.LOAD)
    Page<Author> findAll(Specification<Author> specification, Pageable pageable);

    @Override
    @EntityGraph(value = AUTHOR_DETAIL, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Author> findById(Long aLong);

    @Override
    @EntityGraph(value = AUTHOR_DETAIL, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Author> findOne(Specification<Author> specification);

    static Specification<Author> fullNameLike(String fullName) {
        return ((root, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.like(root.get("fullName"), "%" + fullName + "%"));
    }

}
