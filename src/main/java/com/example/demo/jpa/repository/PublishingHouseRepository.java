package com.example.demo.jpa.repository;

import com.example.demo.jpa.model.Book;
import com.example.demo.jpa.model.PublishingHouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublishingHouseRepository extends JpaRepository<PublishingHouse, Long> {
}
