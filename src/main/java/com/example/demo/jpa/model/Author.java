package com.example.demo.jpa.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.example.demo.jpa.repository.GraphName.AUTHOR_DETAIL;
import static com.example.demo.jpa.repository.GraphName.AUTHOR_LIST;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@NamedEntityGraph(
    name = AUTHOR_LIST
)
@NamedEntityGraph(
    name = AUTHOR_DETAIL,
    attributeNodes = {
        @NamedAttributeNode(value = "books", subgraph = "book-pbhouse")
    },
    subgraphs = {
        @NamedSubgraph(name = "book-pbhouse", attributeNodes = {
            @NamedAttributeNode(value = "publishingHouse")
        })
    }
)
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    private String fullName;

    private LocalDate birthDate;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Book> books = new ArrayList<>();

}
