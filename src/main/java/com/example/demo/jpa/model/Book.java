package com.example.demo.jpa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private LocalDate releaseDate;

    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Author author;

    @ManyToOne(fetch = FetchType.LAZY)
    private PublishingHouse publishingHouse;
}
