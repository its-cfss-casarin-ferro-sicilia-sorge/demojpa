
# Esempi chiamate REST con utilizzo libreria https://github.com/turkraft/spring-filter

GET: localhost:8080/authors?filter=fullName~'Marco*' or fullName:'Giorgia Pagan'

GET: localhost:8080/authors/author?filter=size(books)>0